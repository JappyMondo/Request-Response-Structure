export type BaseResponse = BaseSuccessResponse | BaseErrorResponse;

// 2xx
export interface BaseSuccessResponse {
  data: any;
}

// 1xx, 3xx, 4xx, 5xx
export interface BaseErrorResponse {
  error: BaseResponseError;
}

export interface BaseResponseError {
  type: "SOME_I18N_KEY_FOR_UI";
  message: "Some Humand readable error message for easy debugging, not shown to the user";
  details: any;
}

// TODO: for implementation:
// check that used HTTP Error codes are correct based on REST/HTTP standards