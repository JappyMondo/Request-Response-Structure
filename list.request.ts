export interface ListRequestQueryParams {
    // pagination for list endpoints
    page: number;
    "page-size": number;
    
    // JSON stringified filter based on (something like) sequelize syntax
    filter?: string;

    // sorting
    // JSON Sort Array
    order?: string;

    // additional "edge-case" parameters
    // e.g. for virtual devices
    "include-subaccounts": boolean;
}

// Query Parameters are ALLWAYS kebab-case

// HOW to do Array query params in URL?
// WE WANT THIS! -> ?array[]=1&array[]=2&array[]=3
// and none of these...
// ?array[1]=1&array[2]=2&array[3]=3
// ?array=1,2,3
// ?array=1&array=2&array=3
// ?array<JSON_STRINGIFIED_ARRAY>


export type JsonSort = Array<{
    field: string;
    order: 'ASC' | 'DESC';
}>;