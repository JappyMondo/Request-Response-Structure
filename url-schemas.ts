export type ITEM_IDENTIFIER = string | number;

export type GET_SINGLE_ITEM = `GET /path/to/resource/${ITEM_IDENTIFIER}`;
export type GET_LIST = `GET /path/to/resource`; // + query params
export type CREATE_ITEM = `POST /path/to/resource`;
export type UPDATE_ITEM = `PUT /path/to/resource/${ITEM_IDENTIFIER}`;
// export type UPDATE_ITEM = `PATCH /path/to/resource/${ITEM_IDENTIFIER}`;
export type DELETE_ITEM = `DELETE /path/to/resource/${ITEM_IDENTIFIER}`;

// URI's should always be kebab-case, not a single capital letter