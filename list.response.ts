import { BaseResponse } from "./base.response";

export interface ListResponse extends BaseResponse {
  pagination: ListResponsePagination;
  data: Array<any>;
}

export interface ListResponsePagination {
  page: number;
  /**
   * Actual page size, limited by the server
   * if server limit is 100, and client requested 200, this will be 100
   * if server limit is 100, and client requested 50, this will be 50
   */
  "page-size": number;
  "total-count": number;
}

// IF page is < or > than allowed
// -> throw error

// IF page-size is > than server maximum
// fallback to server maximum

// IF page-size is <=0
// -> throw error